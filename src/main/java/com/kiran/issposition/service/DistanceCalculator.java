package com.kiran.issposition.service;

public interface DistanceCalculator {

    /*Credit Due: This calculation was from https://gist.github.com/yokesharun/c73e290b6d12dd317a37a03370bb08b7#file-find_distance_with_unit-php
    This includes some magic numbers which could be refactored out into constants once we have an understanding of the calculation*/

    static double distance(double lat1, double lng1, double lat2, double lng2) {
        double theta = lng1 - lng2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) *
                Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        return dist;
    }

    static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}
