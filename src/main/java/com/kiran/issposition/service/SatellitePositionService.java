package com.kiran.issposition.service;

import com.kiran.issposition.client.WhereTheIssAtClient;
import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Distance;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.domain.Unit;
import com.kiran.issposition.entity.SatellitePos;
import com.kiran.issposition.exception.PositionNotFoundException;
import com.kiran.issposition.repository.SatellitePositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SatellitePositionService {

    private final WhereTheIssAtClient whereTheIssAtClient;

    private final SatellitePositionRepository satellitePositionRepository;

    @Autowired
    public SatellitePositionService(WhereTheIssAtClient whereTheIssAtClient,
                                    SatellitePositionRepository satellitePositionRepository) {
        this.whereTheIssAtClient = whereTheIssAtClient;
        this.satellitePositionRepository = satellitePositionRepository;
    }

    public List<Catalog> getCatalogOfSatellites() {
        return whereTheIssAtClient.getCatalogOfSatellites();
    }

    public Position getPositionByCatalogId(final Long catalogId) {

        Position  position = whereTheIssAtClient.getPositionByCatalogId(catalogId);

        return Optional.ofNullable(position).map(p -> getPosition(catalogId, p))
            .orElseThrow(PositionNotFoundException::new);
    }

    private Position getPosition(Long catalogId, Position p) {
        satellitePositionRepository.save(new SatellitePos(catalogId, p));
        return new Position(p.getLongitude(),p.getLatitude());
    }

    public Distance calcDistanceFromISS(final Long catalogId, final Double lat, final Double lng) {
        Position position = getPositionByCatalogId(catalogId);
        Double distance = DistanceCalculator.distance(lat,lng,position.getLatitude(),position.getLongitude());
        return Distance.builder().value(distance).unit(Unit.KILOMETRES).build();
    }
}
