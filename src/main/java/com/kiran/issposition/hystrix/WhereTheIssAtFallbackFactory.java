package com.kiran.issposition.hystrix;

import com.kiran.issposition.client.WhereTheIssAtClient;
import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.repository.SatellitePositionRepository;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class WhereTheIssAtFallbackFactory implements FallbackFactory<WhereTheIssAtClient> {

    private final SatellitePositionRepository satellitePositionRepository;

    public WhereTheIssAtFallbackFactory(final SatellitePositionRepository satellitePositionRepository) {
        this.satellitePositionRepository = satellitePositionRepository;
    }


    @Override
    public WhereTheIssAtClient create(Throwable cause) {
        return new WhereTheIssAtClient() {

            @Override
            public List<Catalog> getCatalogOfSatellites() {
                List<Catalog> catalogs = new ArrayList<>();
                catalogs.add(new Catalog("iss", 25544L));
                return catalogs;
            }

            @Override
            public Position getPositionByCatalogId(Long catalogId) {
                return satellitePositionRepository.findById(catalogId)
                        .map(sp -> new Position(sp.getLongitude(), sp.getLatitude()))
                        .orElse(null);
            }
        };
    }
}
