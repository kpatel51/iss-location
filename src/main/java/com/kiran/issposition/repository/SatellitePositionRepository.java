package com.kiran.issposition.repository;

import com.kiran.issposition.entity.SatellitePos;
import org.springframework.data.repository.CrudRepository;

public interface SatellitePositionRepository extends CrudRepository<SatellitePos, Long> {
}