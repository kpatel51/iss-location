package com.kiran.issposition.controller;

import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Distance;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.exception.PositionNotFoundException;
import com.kiran.issposition.service.SatellitePositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = {"/v1/satellites", "/satellites"})
@Slf4j
@Api(value = "Operations for retrieving satellite information")
public class SatellitesController {

    private final SatellitePositionService satellitePositionService;

    @Autowired
    public SatellitesController(SatellitePositionService satellitePositionService) {
        this.satellitePositionService = satellitePositionService;
    }

    @ApiOperation(value = "Retrieve all satellite catalog ids", response = Catalog.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the catalog of satellites"),
            @ApiResponse(code = 404, message = "Unable to retrieve list of satellites")
    })
    @GetMapping
    public List<Catalog> getCatalogOfSatellites() {
        log.debug("retrieving list of satellites");
        return this.satellitePositionService.getCatalogOfSatellites();
    }


    @ApiOperation(value = "Retrieve current position of satellite", response = Position.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the position of the satellite passed in"),
            @ApiResponse(code = 404, message = "The satellite id passed in did not return any position")
    })
    @GetMapping("/position/{catalogId}")
    public Position getPositionByCatalogId(@ApiParam(value = "catalog id to search for", required = true)
                                           @PathVariable final Long catalogId) {
        log.debug("getPositionByCatalogId catalogId: {}", catalogId);
        try {
            return this.satellitePositionService.getPositionByCatalogId(catalogId);
        } catch (PositionNotFoundException exc) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Satellite Position Not Found For: " + catalogId, exc);
        }
    }

    @ApiOperation(value = "Calculate value between ISS and given coordinates", response = Distance.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully calculated the value"),
            @ApiResponse(code = 404, message = "The satellite id passed in did not return any position")
    })
    @GetMapping("/distance/{catalogId}")
    public Distance calcDistanceFromISS(@ApiParam(value = "catalog id to search for", required = true)
                                        @PathVariable final Long catalogId,
                                        @RequestParam final Double lat, @RequestParam final Double lng) {
        log.debug("calcDistanceFromISS latitude: {}, longitude: {}", lat, lng);
        try {
            return this.satellitePositionService.calcDistanceFromISS(catalogId, lat, lng);
        } catch (PositionNotFoundException exc) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Satellite Position Not Found For: " + catalogId, exc);
        }
    }
}
