package com.kiran.issposition.client;

import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.hystrix.WhereTheIssAtFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "wheretheissat", fallbackFactory = WhereTheIssAtFallbackFactory.class)
public interface WhereTheIssAtClient {

    @GetMapping("/v1/satellites")
    List<Catalog> getCatalogOfSatellites();

    @GetMapping("/v1/satellites/{catalogId}")
    Position getPositionByCatalogId(@PathVariable("catalogId") final Long catalogId);
}

