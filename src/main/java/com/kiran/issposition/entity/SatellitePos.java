package com.kiran.issposition.entity;

import com.kiran.issposition.domain.Position;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Data
@Entity
@Table(name = "satellites")
@NoArgsConstructor
public class SatellitePos {

    @Id
    private Long id;
    private Double longitude;
    private Double latitude;

    public SatellitePos(Long id, Position position) {
        this.id = id;
        this.longitude = position.getLongitude();
        this.latitude = position.getLatitude();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SatellitePos that = (SatellitePos) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(latitude, that.latitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, longitude, latitude);
    }
}
