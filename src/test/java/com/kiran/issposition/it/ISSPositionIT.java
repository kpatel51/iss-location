package com.kiran.issposition.it;

import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Distance;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.entity.SatellitePos;
import com.kiran.issposition.repository.SatellitePositionRepository;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class ISSPositionIT {

    private static final String INTERNATIONAL_SPACE_STATION = "iss";
    private static final Long INTERNATIONAL_SPACE_STATION_ID = 25544L;
    private static final Long INVALID_CATALOG_ID = 99999L;

    @ClassRule
    public static final WireMockRule externalSatelliteService = new WireMockRule(options()
            .port(9123)
            .notifier(new ConsoleNotifier(true)));

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private SatellitePositionRepository satellitePositionRepository;

    @Test
    public void retrieveNoradCatalogId() {
        //happy path: retrieve catalog id of ISS calling third party api at https://api.wheretheiss.at/v1/satellites

        //arrange
        externalSatelliteService.stubFor(get("/v1/satellites")
                .willReturn(aResponse().withBodyFile("catalog_ok_response.json").withStatus(200)
                        .withHeader("Content-Type", "application/json")));

        //act
        ResponseEntity<List<Catalog>> response = testRestTemplate.exchange(
                "/satellites",
                HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Catalog>>() {
                });
        List<Catalog> catalogs = response.getBody();

        //assert
        assertThat(catalogs.size(), is(1));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(catalogs.get(0).getName(), is(INTERNATIONAL_SPACE_STATION));
        assertThat(catalogs.get(0).getId(), is(INTERNATIONAL_SPACE_STATION_ID));
    }

    @Test
    public void retrieveCurrentISSPosition() {
        //happy path: test case when we are able to retrieve the current location of the ISS

        //arrange
        externalSatelliteService.stubFor(get("/v1/satellites/"+INTERNATIONAL_SPACE_STATION_ID)
                .willReturn(aResponse().withBodyFile("iss_position_ok_response.json").withStatus(200)
                        .withHeader("Content-Type", "application/json")));

        //act
        ResponseEntity<Position> response = testRestTemplate.exchange(
                "/satellites/position/" + INTERNATIONAL_SPACE_STATION_ID,
                HttpMethod.GET,
                null, Position.class);
        Position issPosition = response.getBody();

        //assert
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(issPosition.getLatitude(), is(50.038553926191));
        assertThat(issPosition.getLongitude(), is(-12.386237511103));

        Optional<SatellitePos> satellitePos = satellitePositionRepository.findById(INTERNATIONAL_SPACE_STATION_ID);
        assertThat(satellitePos.get().getId(),is(INTERNATIONAL_SPACE_STATION_ID));
        assertThat(satellitePos.get().getLatitude(),is(50.038553926191));
        assertThat(satellitePos.get().getLongitude(),is(-12.386237511103));
    }

    @Test
    public void retrieveCurrentISSPositionWithInvalidCatalogId() {
        //sad path: test case when we are unable to retrieve the current location of a catalog id

        //arrange
        externalSatelliteService.stubFor(get("/v1/satellites/" + INVALID_CATALOG_ID)
                .willReturn(aResponse().withBodyFile("iss_position_not_found_response.json").withStatus(404)
                        .withHeader("Content-Type", "application/json")));

        //act
        ResponseEntity<Position> response = testRestTemplate.exchange(
                    "/satellites/position/" + INVALID_CATALOG_ID,
                    HttpMethod.GET,
                    null, Position.class);

        //assert
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));

    }

    @Test
    public void estimateDistanceFromISS() {
        //arrange
        externalSatelliteService.stubFor(get("/v1/satellites/"+INTERNATIONAL_SPACE_STATION_ID)
                .willReturn(aResponse().withBodyFile("iss_position_ok_response.json").withStatus(200)
                        .withHeader("Content-Type", "application/json")));

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/satellites/value/" + INTERNATIONAL_SPACE_STATION_ID)
                .queryParam("lat", "51.507351")
                .queryParam("lng", "-0.127758");

        //act
        ResponseEntity<Distance> response = testRestTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null, Distance.class);
        Distance distanceBetween = response.getBody();

        //assert
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(distanceBetween.getValue(), is(876.2008210699054D));
    }

    @Test
    public void retrieveCurrentISSPositionFromDBUsingFallback() {

        satellitePositionRepository.save(new SatellitePos(INTERNATIONAL_SPACE_STATION_ID, new Position(50.1,-0.10)));

        externalSatelliteService.stubFor(get("/v1/satellites/" + INTERNATIONAL_SPACE_STATION_ID)
                .willReturn(aResponse().withFixedDelay(6000)));

        //act
        ResponseEntity<Position> response = testRestTemplate.exchange(
                "/satellites/position/" + INTERNATIONAL_SPACE_STATION_ID,
                HttpMethod.GET,
                null, Position.class);

        Position issPosition = response.getBody();

        //assert
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(issPosition.getLatitude(), is(-0.10D));
        assertThat(issPosition.getLongitude(), is(50.1D));
    }

    @Test
    public void retrieveCurrentISSDeafultPositionUsingFallbackWhenIdNotInDB() {

        externalSatelliteService.stubFor(get("/v1/satellites/" + INTERNATIONAL_SPACE_STATION_ID)
                .willReturn(aResponse().withFixedDelay(6000)));

        //act
        ResponseEntity<Position> response = testRestTemplate.exchange(
                "/satellites/position/" + INTERNATIONAL_SPACE_STATION_ID,
                HttpMethod.GET,
                null, Position.class);

        Position issPosition = response.getBody();

        //assert
        //assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(issPosition.getLatitude(), is(nullValue()));
        assertThat(issPosition.getLongitude(), is((nullValue())));
    }
}
