package com.kiran.issposition.service;

import com.kiran.issposition.client.WhereTheIssAtClient;
import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Distance;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.domain.Unit;
import com.kiran.issposition.entity.SatellitePos;
import com.kiran.issposition.exception.PositionNotFoundException;
import com.kiran.issposition.repository.SatellitePositionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class SatellitePositionServiceTest {

    private static final String INTERNATIONAL_SPACE_STATION = "iss";
    private static final Long INTERNATIONAL_SPACE_STATION_ID = 25544L;
    private static final Long INVALID_CATALOG_ID = 99999L;

    @Autowired
    private SatellitePositionService satellitePositionService;

    @Mock
    private SatellitePositionRepository satellitePositionRepository;

    @Mock
    private WhereTheIssAtClient whereTheIssAtClient;

    @Before
    public void setUp() {
        satellitePositionService = new SatellitePositionService(whereTheIssAtClient, satellitePositionRepository);
    }

    @Test
    public void returnEmptyCatalogList() {
        when(whereTheIssAtClient.getCatalogOfSatellites()).thenReturn(new ArrayList<>());
        List<Catalog> catalogs = satellitePositionService.getCatalogOfSatellites();
        assertThat(catalogs, is(notNullValue()));
    }

    @Test
    public void returnCatalogList() {
        List<Catalog> catalog = new ArrayList<>();
        catalog.add(Catalog.builder().name("iss").id(INTERNATIONAL_SPACE_STATION_ID).build());
        when(whereTheIssAtClient.getCatalogOfSatellites()).thenReturn(catalog);
        List<Catalog> catalogs = satellitePositionService.getCatalogOfSatellites();
        assertThat(catalogs, is(notNullValue()));
        assertThat(catalogs.size(), is(1));
        assertThat(catalogs.get(0).getId(), is(INTERNATIONAL_SPACE_STATION_ID));
        assertThat(catalogs.get(0).getName(), is(INTERNATIONAL_SPACE_STATION));
    }

    @Test
    public void getPositionByCatalogIdWhenIdIsValid() {
        when(whereTheIssAtClient.getPositionByCatalogId(INTERNATIONAL_SPACE_STATION_ID))
                .thenReturn(Position.builder().latitude(51.123).longitude(-15.456).build());


        Position position = satellitePositionService.getPositionByCatalogId(INTERNATIONAL_SPACE_STATION_ID);

        verify(satellitePositionRepository, times(1)).save(Mockito.any(SatellitePos.class));
        assertThat(position, is(notNullValue()));
        assertThat(position.getLatitude(), is(51.123));
        assertThat(position.getLongitude(), is(-15.456));
    }

    @Test(expected = PositionNotFoundException.class)
    public void getPositionByCatalogIdWhenIdIsInValid() {
        when(whereTheIssAtClient.getPositionByCatalogId(INVALID_CATALOG_ID))
                .thenReturn(null);


        satellitePositionService.getPositionByCatalogId(INVALID_CATALOG_ID);

        verify(satellitePositionRepository, times(0)).save(Mockito.any(SatellitePos.class));
    }

    @Test
    public void calcDistanceFromSatelliteWhenIdIsValid() {
        when(whereTheIssAtClient.getPositionByCatalogId(INTERNATIONAL_SPACE_STATION_ID))
                .thenReturn(Position.builder().latitude(51.123).longitude(-15.456).build());


        Distance distance = satellitePositionService.calcDistanceFromISS(INTERNATIONAL_SPACE_STATION_ID,
                51.507351D, -0.127758D);

        assertThat(distance, is(notNullValue()));
        assertThat(distance.getUnit(), is(Unit.KILOMETRES));
        assertThat(distance.getValue(), is(1064.181224040243D));
    }

    @Test(expected = PositionNotFoundException.class)
    public void calcDistanceFromSatelliteWhenIdIsInValid() {
        satellitePositionService.calcDistanceFromISS(INTERNATIONAL_SPACE_STATION_ID,
                51.507351D, -0.127758D);
    }
}