package com.kiran.issposition.controller;

import com.kiran.issposition.domain.Catalog;
import com.kiran.issposition.domain.Distance;
import com.kiran.issposition.domain.Position;
import com.kiran.issposition.domain.Unit;
import com.kiran.issposition.exception.PositionNotFoundException;
import com.kiran.issposition.service.SatellitePositionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SatellitesController.class)
public class SatellitesControllerTest {

    private static final String INTERNATIONAL_SPACE_STATION = "iss";
    private static final Long INTERNATIONAL_SPACE_STATION_ID = 25544L;
    private static final Long INVALID_CATALOG_ID = 99999L;

    //Use MockMvc to directly test the controller
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SatellitePositionService satellitePositionService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void getCatalogWhenEmpty() throws Exception {
        List<Catalog> catalog = new ArrayList();
        String expectedResponse = objectMapper.writeValueAsString(catalog);
        given(satellitePositionService.getCatalogOfSatellites()).willReturn(catalog);

        //test directly with the controller as apposed to integration test which will test with the spring boot container
        mockMvc.perform(MockMvcRequestBuilders.get("/satellites"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void getCatalogWhenNotEmpty() throws Exception {
        List<Catalog> catalog = new ArrayList();
        catalog.add(Catalog.builder().id(1234L).name("iss").build());
        String expectedResponse = objectMapper.writeValueAsString(catalog);
        given(satellitePositionService.getCatalogOfSatellites()).willReturn(catalog);

        mockMvc.perform(MockMvcRequestBuilders.get("/satellites"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void getPositionByCatalogId() throws Exception {
        Position postition = Position.builder().latitude(12.345).longitude(-56.742).build();
        String expectedResponse = objectMapper.writeValueAsString(postition);
        given(satellitePositionService.getPositionByCatalogId(INTERNATIONAL_SPACE_STATION_ID)).willReturn(postition);
        mockMvc.perform(MockMvcRequestBuilders.get("/satellites/position/" + INTERNATIONAL_SPACE_STATION_ID))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void getPositionByCatalogIdNotFound() throws Exception {
        given(satellitePositionService.getPositionByCatalogId(INVALID_CATALOG_ID)).willThrow(new PositionNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/satellites/position/" + INVALID_CATALOG_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void calcDistanceFromISS() throws Exception {
        Distance distance = Distance.builder().unit(Unit.KILOMETRES).value(12345.021D).build();
        String expectedResponse = objectMapper.writeValueAsString(distance);
        given(satellitePositionService.calcDistanceFromISS(eq(INTERNATIONAL_SPACE_STATION_ID), eq(50.12), eq(-12.32)))
                .willReturn(distance);
        mockMvc.perform(MockMvcRequestBuilders.get("/satellites/distance/" + INTERNATIONAL_SPACE_STATION_ID)
                .param("lat", "50.12")
                .param("lng", "-12.32")
        )
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void calcDistanceFromISSWhenCatalogIdNotFound() throws Exception {
        given(satellitePositionService.calcDistanceFromISS(eq(INVALID_CATALOG_ID), eq(50.12), eq(-12.32)))
                .willThrow(new PositionNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/satellites/distance/" + INVALID_CATALOG_ID)
                .param("lat", "50.12")
                .param("lng", "-12.32")
        ).andExpect(status().isNotFound());
    }
}