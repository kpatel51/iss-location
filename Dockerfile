FROM centos

RUN yum install -y java

VOLUME /tmp
ADD /target/iss-position*.jar iss-position.jar
ENV LANG en_GB.UTF-8
ENV LANGUAGE en_GB
ENV LC_ALL en_GB.UTF-8
RUN sh -c 'touch /iss-position.jar'
ENTRYPOINT ["java","-jar","/iss-position.jar"]